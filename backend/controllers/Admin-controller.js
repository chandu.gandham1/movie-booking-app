import Admin from "../models/Admin.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

// Signup
export const addAdmin = async (req, res, next) => {
    const { email, password } = req.body;
    if (!email || !password) {
        return res.status(422).json({ message: "Invalid Inputs" });
    }

    try {
        let existingAdmin = await Admin.findOne({ email });
        if (existingAdmin) {
            return res.status(400).json({ message: "Admin already exists" });
        }

        const hashedPassword = bcrypt.hashSync(password);
        const admin = new Admin({ email, password: hashedPassword });
        await admin.save();

        return res.status(201).json({ admin });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: "Unable to store admin" });
    }
};

// Login
export const adminLogin = async (req, res, next) => {
    const { email, password } = req.body;
    if (!email || !password) {
        return res.status(422).json({ message: "Invalid Inputs" });
    }

    try {
        const existingAdmin = await Admin.findOne({ email });
        if (!existingAdmin) {
            return res.status(400).json({ message: "Admin not found" });
        }

        const isPasswordCorrect = bcrypt.compareSync(password, existingAdmin.password);
        if (!isPasswordCorrect) {
            return res.status(400).json({ message: "Incorrect Password" });
        }

        const token = jwt.sign({ id: existingAdmin._id }, process.env.SECRET_KEY, {
            expiresIn: "7d",
        });

        return res.status(200).json({ message: "Authentication Complete", token, id: existingAdmin._id });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: "Internal server error" });
    }
};

// Get all admins
export const getAdmins = async (req, res, next) => {
    try {
        const admins = await Admin.find();
        return res.status(200).json({ admins });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: "Internal Server Error" });
    }
};

// Get admin by ID
export const getAdminById = async (req, res, next) => {
    const id = req.params.id;
    try {
        const admin = await Admin.findById(id).populate("addedMovies");
        if (!admin) {
            return res.status(404).json({ message: "Admin not found" });
        }
        return res.status(200).json({ admin });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: "Internal server error" });
    }
};

// import Admin from "../models/Admin.js";
// import bcrypt from "bcryptjs";
// import jwt from "jsonwebtoken";


// //Signup
// export const addAdmin = async (req,res,next) => {
//     const {email, password} =req.body;
//     if (!email || !password) {
//         return res.status(422).json({ message: "Invalid Inputs" });
//     }

//     let existingAdmin;
//     try{
//         existingAdmin = await Admin.findOne({email});
//      } catch(err){
//         return console.log(err)
//     }

//     if(existingAdmin){
//         return res.status(400).json({mesage:"Admin already exists"})
//     }

//     let admin;
//     const hashedPassword = bcrypt.hashSync(password);
//     try{
//         admin = new Admin({email,password: hashedPassword,});
//         admin = await admin.save();
//      } 
//     catch(err){
//         return console.log(err)
//     }
//     if(!admin) {
//         return res.status(500).json({message: "Unable to store admin"})
//     }
//     return res.status(201).json({admin})

// };


// //login

// export const adminLogin = async(req,res,next) =>  {
//     const {email, password} =req.body;
//     if (!email || !password) {
//         return res.status(422).json({ message: "Invalid Inputs" });
//     }
//     let existingAdmin;
//     try{
//         existingAdmin = await Admin.findOne({email});
//      } catch(err) {
//         return console.log(err)
//     }
//     if(!existingAdmin){
//         return res.status(400).json({message: "Admin not found"})
//     }
//     const isPasswordCorrect = bcrypt.compareSync(password, existingAdmin.password);

//     if(!isPasswordCorrect){
//         return res.status(400).json({message:"Incorrect Password"});
//     }

//     const token = jwt.sign({id:existingAdmin._id},process.env.SECRET_KEY,{
//         expiresIn: "7d",
//     });

//     return res.status(200).json({message: "Authentication Complete",token,id:existingAdmin._id})

    

// };

// export const getAdmins = async(req,res,next) => {
//     let admins;
//     try{
//         admins = await Admin.find();
//     } catch(err) {
//         return console.log(err)
//     } 
//     if(!admins){
//         return res.status(500).json({message: "Internal Server Error"})
//     }
//     return res.status(200).json({admins});
// };
// export const getAdminById = async (req, res, next) => {
//     const id = req.params.id;
  
//     try {
//         const admin = await Admin.findById(id).populate("addedMovies");
      
//       if (!admin) {
//         return res.status(404).json({ message: "Admin not found" });
//       }
  
//       return res.status(200).json({ admin });
//     } catch (err) {
//       console.log(err);
//       return res.status(500).json({ message: "Internal server error" });
//     }
//   };
  