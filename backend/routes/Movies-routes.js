import express from 'express';
import { addMovie, getAllMovies, getMovieById } from '../controllers/Movie-controller.js';

const movieRouter = express.Router();

// Route to get all movies
movieRouter.get('/', getAllMovies);

// Route to get a specific movie by ID
movieRouter.get('/:id', getMovieById);

// Route to add a new movie
movieRouter.post('/', addMovie);

export default movieRouter;
