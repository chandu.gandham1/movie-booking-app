import React from 'react'
import AuthForm from './AuthForm'
import { useDispatch } from 'react-redux';
import { sendUserAuthRequest } from '../../api/api';
import { adminActions, userActions } from '../../store';
import { Navigate, useNavigate } from 'react-router-dom';

const Auth = () => {
  const navigate=useNavigate();
  const  dispatch = useDispatch();
  const onResreceived = (data)=>{
    console.log('data', data.data.id);
    dispatch(userActions.login())
    localStorage.setItem("userId",data.data.id)
    navigate("/")
  }
  const handleFormSubmit = (isAdmin) => {
    if(isAdmin){
      dispatch(adminActions.login());    // Dispatch the login action
      navigate("/");
    }
    else{
      dispatch(userActions.login());    // Dispatch the login action
      navigate("/");
    }
  };
  const getData=(data)=>{
    sendUserAuthRequest(data.inputs,data.signup)
    .then((res)=>{console.log('fghjk',res);
     onResreceived(res)
  })
    .then(()=>{dispatch(userActions.login())})
    .catch((err)=>console.log(err))
  }
  return (
    <div>
      <AuthForm onSubmit={getData} isAdmin={false} handleFormSubmit={handleFormSubmit} />
    </div>
  )
}

export default Auth
