import React, { useState } from "react";
import {
  Box,
  TextField,
  IconButton,
  FormLabel,
  Dialog,
  Typography,
  Button,
  Modal
} from "@mui/material";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import { Link, useNavigate } from "react-router-dom";
const labelStyle = { mt: 1, mb: 1 };
const AuthForm = ({ onSubmit, isAdmin ,handleFormSubmit}) => {
  const [inputs, setInputs] = useState({
    name: "",
    email: "",
    password: "",
  });
  const navigate = useNavigate();
  const [showPopup, setShowPopup] = useState(false);
  const handleChange = (e) => {
    setInputs((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit({ inputs, signup: isAdmin ? false : isSignup });
    handleFormSubmit(isAdmin);
    setShowPopup(true);

  };
  const [isSignup, setIsSignup] = useState(false);
  return (
    <Dialog open={true}>
      <Box sx={{ ml: "auto", padding: 1 }}>
        <IconButton LinkComponent={Link} to="/">
          <CloseRoundedIcon />
        </IconButton>
      </Box>
      <Typography variant="h4" textAlign={"center"}>
        Login
      </Typography>
      <form onSubmit={handleSubmit}>
        <Box
          padding={6}
          margin={"auto"}
          display={"flex"}
          width={400}
          flexDirection={"column"}
          alignContent={"center"}
          justifyContent={"center"}
        >
          {!isAdmin && isSignup && (
            <>
              {" "}
              <FormLabel sx={labelStyle}>Name</FormLabel>
              <TextField
                value={inputs.name}
                onChange={handleChange}
                margin="normal"
                variant="standard"
                type={"text"}
                name="name"
              />
            </>
          )}
          <FormLabel sx={labelStyle}>Email</FormLabel>
          <TextField
            value={inputs.email}
            onChange={handleChange}
            margin="normal"
            variant="standard"
            type={"email"}
            name="email"
          />
          <FormLabel sx={labelStyle}>Password</FormLabel>
          <TextField
            value={inputs.password}
            onChange={handleChange}
            margin="normal"
            variant="standard"
            type={"password"}
            name="password"
            autoComplete="current-password"

            // autoComplete="current-password"
          />
          <Button
            sx={{ mt: 1, borderRadius: 10, bgcolor: "#2b2d42" }}
            type="submit"
            fullWidth
            variant="contained"
          >
            {isSignup ? "Signup" : "Login"}
          </Button>
          {!isAdmin && (
            <Button
              onClick={() => setIsSignup(!isSignup)}
              sx={{ mt: 1, borderRadius: 10 }}
              fullWidth
            >
              {isSignup ? "Switch to Login" : "Switch to Signup"}
            </Button>
          )}
        </Box>
      </form>
      <Modal open={showPopup} onClose={() => setShowPopup(false)}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 400,
            bgcolor: "background.paper",
            border: "2px solid #000",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Typography variant="h6" component="h2">
            Logged in successfully!
          </Typography>
          {/* Additional content can be added here */}
        </Box>
      </Modal>
    </Dialog>
  );
};

export default AuthForm;
