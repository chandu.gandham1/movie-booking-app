import React from 'react'
import { useDispatch } from 'react-redux';
import AuthForm from './AuthForm'
import { sendAdminAuthRequest } from '../../api/api.js';
import { adminActions } from '../../store';
import { useNavigate } from "react-router-dom";

const Admin = () => {
  const navigate= useNavigate()
  const  dispatch = useDispatch();
  const onResreceived = (data)=>{
    console.log(data);
    dispatch(adminActions.login())
    localStorage.setItem("adminId",data.id)
    localStorage.setItem("token",data.token)
    navigate("/");
  }
  const getData=(data)=>{
    console.log("Admin",data);
     sendAdminAuthRequest(data.inputs)
     .then(onResreceived)
    .catch((err)=>console.log(err))
  }
  return (
    <div>
      <AuthForm onSubmit={getData} isAdmin={true}/>
    </div>
  )
}

export default Admin
