import React, { useEffect, useState } from "react";
import { Box, Typography} from "@mui/material";
import { getAllMovies } from "../../api/api";
import Movieitem from "./Movieitem";

const Movies = () => {
  const [movies, setMovies] = useState();
  useEffect(() => {
    getAllMovies()
      .then((data) => setMovies(data.movies))
      .catch((err) => console.log(err));
  }, []);
  return (
    <Box Margin={"auto"} marginTop={4}>
      {" "}
      <Typography
        margin={"auto"}
        varient="h4"
        padding={2}
        width={"40%"}
        bgcolor={"#900C3F"}
        color={"white"}
        textAlign={"center"}
      >
        All Movies
      </Typography>
      <Box margin={"auto"}
        marginTop={5}
        display={"flex"}
        width={"100%"}
        flexWrap={"wrap"}
       justifyContent ={"flex-start"}>
    {movies && movies.map((movie, index) => (
      <Movieitem
      key={index}
      id={movie._id}
      title={movie.title}
      posterUrl={movie.posterUrl}
      releaseDate={movie.releaseDate}/>
    ))}

       </Box>
    </Box>
  );
};

export default Movies;
