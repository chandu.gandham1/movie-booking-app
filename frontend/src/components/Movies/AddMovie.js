import React, { useState } from "react";
import {
  Box,
  Typography,
  FormLabel,
  TextField,
  Button,
  Checkbox,
} from "@mui/material";
import { addMovie } from "../../api/api";
const labelProps = {
  mt: 1,
  mb: 1,
};
const AddMovie = () => {
  const [Inputs, setInputs] = useState({
    title: "",
    description: "",
    posterUrl: "",
    releaseDate: "",
    featured: false,
  });
  const [actors,setActors]=useState([]);
  const [actor,setActor]=useState([""]);
  const handleChange = (e) => {
    setInputs((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };
  const handleSubmit = (e) => {
    e.preventDefault();
     console.log(Inputs,actors);
    addMovie({...Inputs,actor:actors}).then((res) => console.log(res))
    .catch((err) => console.log(err));
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Box
          width={"50%"}
          padding={10}
          margin="auto"
          display={"flex"}
          flexDirection="column"
          boxShadow={"10px 10px 20px #ccc"}
        >
          <Typography textAlign={"center"} variant="h5" fontFamily={"verdana"}>
            Add New Movie
          </Typography>

          <FormLabel sx={labelProps}>Title </FormLabel>
          <TextField
            value={Inputs.title}
            onChange={handleChange}
            name="title"
            variant="standard"
            margin="normal"
          />

          <FormLabel sx={labelProps}>Description</FormLabel>
          <TextField
            value={Inputs.description}
            onChange={handleChange}
            name="description"
            variant="standard"
            margin="normal"
          />

          <FormLabel sx={labelProps}>Poster URL</FormLabel>
          <TextField
            value={Inputs.posterUrl}
            onChange={handleChange}
            name="posterUrl"
            variant="standard"
            margin="normal"
          />

          <FormLabel sx={labelProps}>release Date</FormLabel>
          <TextField
            type="date"
            value={Inputs.releaseDate}
            onChange={handleChange}
            name="releaseDate"
            variant="standard"
            margin="normal"
          />
          <FormLabel sx={labelProps}>Actor</FormLabel>
          <Box display={"flex"}>
            <TextField value={actor} name="actor" onChange={(e)=>setActor(e.target.value)} variant="standard" margin="normal" />
            <Button onClick={()=>{setActors([...actors,actor]);
            setActor("");
            }}>Add</Button>
          </Box>
          <FormLabel sx={labelProps}>Featured</FormLabel>
          <Checkbox
            name="featured"
            checked={Inputs.featured}
            onClick={(e)=>setInputs((prevState)=>({...prevState,featured:e.target.checked}))}
            sx={{ mr: "auto" }}
          />
          <Button
            type="submit"
            varient="contained"
            sx={{
              margin: "auto",
              width: "30%",
              bgcolor: "#2b2d42",
              ":hover": { bgcolor: "12121" },
            }}
          >
            Add New Movie
          </Button>
        </Box>
      </form>
    </div>
  );
};

export default AddMovie;
