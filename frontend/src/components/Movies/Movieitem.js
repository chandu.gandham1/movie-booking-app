import React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';

const Movieitem = ({ id, title, posterUrl, releaseDate }) => {
  return (
    <div style={{ margin: '10px', display: 'inline-block' }}>
      <Card sx={{ margin: 2,  width: 250, height: 320, borderRadius: 5, ":hover": {
        boxShadow: "10px 10px 20px #ccc"
      } }}>
        <CardMedia
          component="img"
          height="50%"
          image={posterUrl} 
          alt={title} 
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">{title}</Typography>
          <Typography variant="body2" color="text.secondary">
            {new Date(releaseDate).toDateString()}
          </Typography>
        </CardContent>
        <CardActions>
          <Button LinkComponent={Link} to={`/booking/${id}`} sx={{margin:"auto"}} size="small">Book</Button>
        </CardActions>
      </Card>
    </div>
  );
}

export default Movieitem;
