import React, { useState, useEffect } from "react";
import {
  AppBar,
  Toolbar,
  Autocomplete,
  TextField,
  Tabs,
  Tab,
  IconButton,
} from "@mui/material";
import MovieIcon from "@mui/icons-material/Movie";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Box";

import { getAllMovies } from "../api/api";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { userActions, adminActions } from "../store";
import Movieitem from "./Movies/Movieitem"; // Import Movieitem component

const Header = () => {
  const navigate= useNavigate()
  const dispatch = useDispatch();
  const isAdminloggedIn = useSelector((state) => state.admin.isLoggedIn);
  const isUserLoggedIn = useSelector((state) => state.user.isLoggedIn);

  const [value, setValue] = useState(0);
  const [movies, setMovies] = useState([""]);
  const [selectedMovie,setselectedMovie]=useState()

  useEffect(() => {
    getAllMovies()
      .then((data) => setMovies(data.movies))
      .catch((err) => console.log(err));
  }, []);
    
  
  const logout = (isAdmin) => {
    dispatch(isAdmin ? adminActions.logout() : userActions.logout());
  };
  const handleChange=(e,val)=>{
    const movie=movies.find((m)=>m.title === val)
    console.log(isUserLoggedIn);
    if(isUserLoggedIn){
      navigate(`/booking/${movie._id}`)
    }
  }

  return (
    <div>
      <AppBar position="sticky" sx={{ bgcolor: "#2b2d42" }}>
        <Toolbar>
          <Box sx={{ display: "flex", alignItems: "center", width: "100%" }}>
            <IconButton LinkComponent={Link} to="/">
            <MovieIcon sx={{ color: 'white' }}/>
            </IconButton>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1, ml: 1 }}>
                BookmyShow
              </Typography>

          </Box>
          <Box width={"25%"} margin={"auto"}>
            <Autocomplete
              onChange={handleChange}
              id="free-solo-demo"
              freeSolo
              options={movies.map((movie) => movie.title)}
              renderInput={(params) => (
                <TextField
                  sx={{ input: { color: "white" } }}
                  {...params}
                  placeholder="Search Movies"
                />
              )}
            />
          </Box>
          <Box display={"flex"}>
            <Tabs
               sx={{ color: "white" }}
               textColor="inherit"
               indicatorColor=""
              value={value}
              onChange={(e, val) => setValue(val)}
            >
              <Tab sx={{ color: "white" }} LinkComponent={Link} to="/movies" label="Movies" />
              {!isAdminloggedIn && !isUserLoggedIn && (
                <>
                  <Tab  sx={{ color: "white" }} LinkComponent={Link} to="/admin" label="Admin" />
                  <Tab  sx={{ color: "white" }} LinkComponent={Link} to="/auth" label="User " />
                </>
              )}
              {isUserLoggedIn && (
                <>
                  <Tab  sx={{ color: "white" }} LinkComponent={Link} to="/user" label="Profile" />
                  <Tab
                    onClick={() => logout(false)}
                    LinkComponent={Link}
                    to="/"
                    sx={{ color: "white" }}
                    label="Logout"
                  />
                </>
              )}
              {isAdminloggedIn && (
                <>
                  <Tab  sx={{ color: "white" }} LinkComponent={Link} to="/add" label="Add Movie" />
                  <Tab sx={{ color: "white" }} LinkComponent={Link} to="/user-admin" label="Profile" />
                  <Tab
                    onClick={() => logout(true)}
                    sx={{ color: "white", marginRight: "20px"  }}
                    LinkComponent={Link}
                    to="/"
                    label="Logout"
                  />
                </>
              )}
            </Tabs>
          </Box>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
