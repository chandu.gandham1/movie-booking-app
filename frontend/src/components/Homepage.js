import React,{useEffect,useState} from "react";
import { Box, Typography,Button,Paper } from "@mui/material";
import Movieitem from "./Movies/Movieitem";
import { Link } from "react-router-dom";
import { getAllMovies } from "../api/api";
import Carousel from 'react-material-ui-carousel';


const Homepage = () => {
    const [movies,setMovies]=useState([]);
    useEffect(() => {
        getAllMovies()
          .then(data => setMovies(data.movies))
          .catch(err => console.log(err));
      }, []); 
  return (
    <Box width={"100%"} height="100%" margin="auto" marginTop={2}>
      <Box margin={"auto"} width="80%" height={"50vh"} padding={2}>
      <Carousel animation="slide">
                    {movies && movies.slice(3,6 ).map((movie, index) => (
                        <Paper key={index} sx={{ width: "100%", height: "500px" }}>
                        <img
                          src={movie.posterUrl}
                          alt={movie.title}
                          style={{
                            width: "100%",
                            height: "100%",
                            objectFit: "cover",
                          }}
                          className="carousel-image"
                        />
                      </Paper>
                    ))}
                </Carousel>
      </Box>
      <Box paddingTop={"20%"} margin={"auto"}>
    <Typography variant="h4" textAlign={"center"}>Latest Movies</Typography>
      </Box>
<Box 
  display={"flex"}
  width={"80%"}
  justifyContent={"center"}
  flexWrap={"wrap"}
  margin={"auto"}
>
  {movies && movies.map((movie, index) => (
    <Movieitem 
      key={index}
      id={movie.id}
      title={movie.title}
      posterUrl={movie.posterUrl}
      releaseDate={movie.releaseDate}
    />
  ))}
</Box>

      <Box display={"flex"} padding={5} margin={"auto"}>
        <Button LinkComponent={Link} to="/movies" variant="outlined" sx={{margin:"auto", color:"#2b2d42"}}>View All Movies</Button>
      </Box>
    </Box>
  );
};

export default Homepage;
