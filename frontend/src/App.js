import Header from "./components/Header";
import Homepage from "./components/Homepage";
import { useSelector } from 'react-redux';
import { Route,Routes } from "react-router-dom";
import Movies from "./components/Movies/Movies";
import Admin from "./components/Auth/Admin";
import { useDispatch } from 'react-redux';
import Auth from "./components/Auth/Auth";
import { useEffect } from "react";
import { userActions, adminActions } from './store';
import UserProfile from "./profile/UserProfile.js";
import AddMovie from "./components/Movies/AddMovie";
import AdminProfile from "./profile/AdminProfile";
import Booking from "./components/Bookings/Booking.js";

function App() {
  const dispatch = useDispatch();
  const isAdminLoggedIn = useSelector((state)=>state.admin.isLoggedIn)
  const isUserLoggedIn = useSelector((state)=>state.user.isLoggedIn)
  console.log(" isAdminLoggedIn",isAdminLoggedIn);
  console.log(" isUserLoggedIn",isUserLoggedIn);
  useEffect(()=>{
    if(localStorage.getItem("userId")){
      dispatch(userActions.login())
    }
    else if(localStorage.getItem("adminId")){
      dispatch(adminActions.login())
    }
  },[])
 
  return (
    <div className="App">
      <Header/>
      <section>
        <Routes>
          <Route path='/' element={<Homepage/>}/>
          <Route path='/movies' element={<Movies/>}/>
          <Route path='/admin' element={<Admin/>}/>
          <Route path='/auth' element={<Auth/>}/>
          <Route path='/user' element={<UserProfile/>}/>
          <Route path='/add' element={<AddMovie/>}/>
          <Route path='/user-admin' element={<AdminProfile/>}/>

          <Route path='/booking/:id' element={<Booking/>}/>
        </Routes>
      </section>
        
      
    </div>
  );
}

export default App;
