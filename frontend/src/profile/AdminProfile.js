import React, { Fragment, useEffect, useState } from "react";
import { getAdminById } from "../api/api.js";
import Box from "@mui/material/Box";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import {  Typography } from "@mui/material";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";


const AdminProfile = () => {
    const [admin, setAdmin] = useState("");
    useEffect(() => {
      
      getAdminById()
        .then((res) => setAdmin(res.admin))
        .catch((err) => console.log(err));
    }, []);
    console.log(admin);
    return (
      <div>
        <Box width={"100%"} display="flex">
          <Fragment>
            {" "}
            {admin && (
              <Box
                flexDirection={"column"}
                justifyContent="center"
                alignItems={"center"}
                width={"30%"}
                padding={3}
              >
                <AccountCircleIcon
                  sx={{ fontSize: "10rem", textAlign: "center", ml: 15 }}
                />
                <Typography
                  padding={1}
                  width={"auto"}
                  textAlign={"center"}
                  border={"1px solid #ccc"}
                  borderRadius={6}
                >
                  Name:{admin.name}
                </Typography>
                <Typography
                  mt={1}
                  padding={1}
                  width={"auto"}
                  textAlign={"center"}
                  border={"1px solid #ccc"}
                  borderRadius={6}
                >
                  Email:{admin.email}
                </Typography>
              </Box>
            )}
            
            {admin && admin.addedMovies.length > 0 &&  (
              <Box width={"70%"} display="flex" flexDirection={"column"}>
                <Typography
                  variant="h3"
                  fontFamily={"verdana"}
                  textAlign={"center"}
                  padding={2}
                >
                  Added Movies
                </Typography>
                <List>
                  {admin.addedMovies.map((movie, index) => (
                    <ListItem
                      key={index}
                      sx={{
                        bgcolor: "#00d386",
                        color: "white",
                        textAlign: "center",
                        margin: 1,
                        
                      }}
                    >
                      <ListItemText
                        sx={{ margin: 1, width: "auto", textAlign: "center" }}
                      >
                        Movie: {movie.title}
                      </ListItemText>
        
                    </ListItem>
                  ))}
                </List>
              </Box>
            )}
          </Fragment>
        </Box>
      </div>
    );
  };
  

export default AdminProfile
