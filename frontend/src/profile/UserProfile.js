import React, { Fragment, useEffect, useState } from "react";
import getUserBooking, { deleteBooking } from "../api/api";
import Box from "@mui/material/Box";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { IconButton, Typography } from "@mui/material";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

const UserProfile = () => {
  const [bookings, setBookings] = useState([]);
  const [user, setUser] = useState({ name: "", email: "" }); // Initialize with default values or empty strings

  useEffect(() => {
    getUserBooking()
      .then((res) => {
        if (res) {
          setBookings(res.bookings);
          setUser(res.user);
        }
      })
      .catch((err) => console.log(err));
  }, []);
console.log("userrrr", user)
  const handleDelete = (id) => {
    deleteBooking(id)
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  return (
    <div>
      <Box width={"100%"} display="flex">
        {user || (
          <Fragment>
            <Box
              flexDirection={"column"}
              justifyContent="center"
              alignItems={"center"}
              width={"30%"}
              padding={3}
            >
              <AccountCircleIcon
                sx={{ fontSize: "10rem", textAlign: "center", ml: 3 }}
              />
              <Typography
                padding={1}
                width={"auto"}
                textAlign={"center"}
                border={"1px solid #ccc"}
                borderRadius={6}
              >
                Name: {user.name}
              </Typography>
              <Typography
                mt={1}
                padding={1}
                width={"auto"}
                textAlign={"center"}
                border={"1px solid #ccc"}
                borderRadius={6}
              >
                Email: {user.email}
              </Typography>
            </Box>
            {bookings && (
              <Box width={"70%"} display="flex" flexDirection={"column"}>
                <Typography
                  variant="h3"
                  fontFamily={"verdana"}
                  textAlign={"center"}
                  padding={2}
                >
                  Bookings
                </Typography>
                <List>
                  {bookings.map((booking, index) => (
                    <ListItem
                      key={index}
                      sx={{
                        bgcolor: "#00d386",
                        color: "white",
                        textAlign: "center",
                        margin: 1,
                      }}
                    >
                      <ListItemText
                        sx={{ margin: 1, width: "auto", textAlign: "center" }}
                      >
                        {/* Check if booking.movie exists before accessing its properties */}
                        Movie: {booking.movie ? booking.movie.title : 'Unknown'}
                      </ListItemText>
                      <ListItemText
                        sx={{ margin: 1, width: "auto", textAlign: "left" }}
                      >
                        Seat: {booking.seatNumber}
                      </ListItemText>
                      <ListItemText
                        sx={{ margin: 1, width: "auto", textAlign: "center" }}
                      >
                        Date: {new Date(booking.releaseDate).toLocaleString()}
                      </ListItemText>
                      <IconButton
                        onClick={() => handleDelete(booking._id)}
                        color="error"
                      >
                        <DeleteForeverIcon />
                      </IconButton>
                    </ListItem>
                  ))}
                </List>
              </Box>
             )} 
          </Fragment>
        )}
      </Box>
    </div>
  );
};

export default UserProfile;
