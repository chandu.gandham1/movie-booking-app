import axios from "axios";

export const getAllMovies = async () => {
  try {
    const res = await axios.get("/movie");
    
    if (!res || !res.status || res.status !== 200) {
      console.log("No Data or Invalid Response");
      return null; // Return null or some other value indicating no data
    }

    return res.data;
  } catch (error) {
    console.log("Error fetching movies:", error.message);
    throw error; // Re-throw the error to be caught by the caller
  }
};
export const sendUserAuthRequest = async (data, signup) => {
  let res;
  try {
      res = await axios.post(`/user/${signup ? "signup" : "login"}`, {
          name: signup ? data.name : "",
          email: data.email,
          password: data.password,
      });

      console.log('abccc ', res)
      localStorage.setItem('userId', res.data.id)
      return res.data;
  } catch (err) {
      console.log(err);
  }

  if (res && (res.status !== 200 && res.status !== 201)) {
      console.log("Unexpected error occurred");
  }
  const resData = res ? await res.data : null;
  return resData;
};

  export const sendAdminAuthRequest = async (data) => {
    const res = await axios.post("/admin/login", {
      email: data.email,
      password: data.password,
    })
    .catch((err)=>console.log(err))

    if (res.status !== 200) {
      console.log("Unexpected error occurred");
    }
    const resData = await res.data;
    return resData;
  } ;
export const getMovieDetails = async(id)=>{
  const res = await axios.get(`/movie/${id}`).catch((err)=> console.log(err));
  if(res.status!==200){
    return console.log("Unexpected error occurred");
    
  }
  const resData=await res.data;
    return resData;
}
export const newBooking = async (data) => {
  try {
    const res = await axios.post(`/booking`, {
      movie: data.movie,
      seatNumber: data.seatNumber,
      date: data.date,
      user: localStorage.getItem("userId"),
    });

    if (res.status === 201) {
      return res.data;
    } else {
      throw new Error("Unexpected error occurred");
    }
  } catch (error) {
    console.log("Error creating new booking:", error.message);
    throw error; // Propagate the error to be caught by the caller
  }
};

const getUserBooking = async () => {
  try {
    const id = localStorage.getItem("userId");
    const res = await axios.get(`/user/bookings/${id}`);
    
    if (!res || !res.status || res.status !== 200) {
      console.log("Error fetching user bookings:", res.statusText);
      return null; // Return null or some other value indicating no data
    }
    console.log(res);

    return res.data;
  } catch (error) {
    console.log("Error fetching user bookings:", error.message);
    throw error; // Re-throw the error to be caught by the caller
  }
};

export default getUserBooking;
export const deleteBooking = async(id)=>{
  const res = await axios.delete(`/booking/${id}`).catch((err)=>console.log(err))
  if(res.status!==200){
    return console.log("Unexpected error occurred");
  }
  const resData=await res.data;
  return resData
}
export const getUserDetails=async()=>{
  const id = localStorage.getItem("userId")
  const res = await axios.get(`user/${id}`).catch((err)=>console.log(err))
  if(res.status!==200){
    return console.log("Unexpected error occurred");
  }
  const resData=await res.data;
  return resData

}
export const addMovie = async(data)=>{
  const res = await axios.post("/movie",{
    title:data.title,
    description:data.description,
    releaseDate: data.releaseDate,
    posterUrl: data.posterUrl,
    featured: data.featured,
    cast: data.actors,
    admin: localStorage.getItem("adminId"),
  },{
    headers:{
      Authorization:`Bearer ${localStorage.getItem("token")}`,
    }
  }).catch((err)=>console.log(err))
  if(res.status!==201){
    return console.log("Unexpected error occurred");
  }
  const resData=await res.data;
  return resData
}
export const getAdminById = async()=>{
  const adminId=localStorage.getItem("adminId")
  
  const res = await axios.get(`/admin/${adminId}`).catch((err)=>console.log(err))
  if(res.status!==200){
    return console.log("Unexpected error occurred");
  }
  const resData=await res.data;
  return resData
}

// export const getAllMovies = async () => {
//   try {
//     const res = await axios.get("http://localhost:7120/movie");
    
//     if (res.status !== 200) {
//       throw new Error("Failed to fetch data");
//     }

//     return res.data;
//   } catch (error) {
//     console.log("Error fetching movies:", error.message);
//     throw error;
//   }
// };